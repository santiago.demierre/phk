# -*- coding: utf-8 -*-
import app.phk.recorder as recorder

from app.script import RunPython, this_file

THIS_FILE = this_file(__file__)

start_keys = "<ctrl><alt>="
stop_keys = "<ctrl><alt>-"
play_keys = "<ctrl><alt>0"

hotkeys = [[RunPython, start_keys, THIS_FILE, "Record macro"]]

def main() -> None:
    """Start the macro recorder."""
    try:
        recorder.start(stop_keys, play_keys)
    except KeyboardInterrupt:
        print("MacroRecorder stopped with Ctrl-c")


if __name__ == "__main__":
    main()
