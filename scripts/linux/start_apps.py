# -*- coding: utf-8 -*-
from app.script import Run

hotkeys = []

hotkeys.append([Run, "<shift><cmd>c", "chromium-browser&", "Chromium"])
hotkeys.append([Run, "<shift><cmd>f", "thunar&", "Thunar"])
hotkeys.append([Run, "<shift><cmd>g", "gimp&", "Gimp"])
hotkeys.append([Run, "<shift><cmd>o", "libreoffice&", "Office"])
hotkeys.append([Run, "<shift><cmd>t", "xfce-terminal&", "Terminal"])
hotkeys.append([Run, "<shift><cmd>w", "firefox&", "Firefox"])
