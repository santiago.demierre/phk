# -*- coding: utf-8 -*-
import socket
import re
import uuid

import app.config as config
log = config.macro_logger


class Network:
    """Network related functions"""

    def __init__(self) -> None:
        pass

    @staticmethod
    def hostname() -> str:
        """
        Returns the hostname of the machine.
        """
        hostname = socket.gethostname()
        log.debug("Hostname: [{}]".format(hostname))
        return hostname

    @staticmethod
    def mac_address() -> str:
        """
        Returns the MAC address of the computer.
        Can be used to identify a computer in the network
        """
        mac = ':'.join(re.findall('..', '%012x' % uuid.getnode()))
        log.debug("MAC address: [{}]".format(mac))
        return mac

    @staticmethod
    def has_internet(host: str="www.google.com", port: int=80) -> bool:
        """
        Checks if there is an internet connection available.

        :param host: Name of the host. Default google.com
        :param port: The port to check. Default =80
        :return: True or False
        """
        try:
            socket.create_connection((host, port))
            return True
        except OSError:
            pass
        return False

