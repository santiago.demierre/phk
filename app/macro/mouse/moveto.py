# -*- coding: utf-8 -*-
from pyautogui import position, size, moveTo
from app.macro.mouse.log import log_action


class MoveTo:

    @staticmethod
    def coord(coord: tuple, seconds: int=0) -> None:
        """
        Move the mouse to a coordinate. No clicking.
        :param coord: The coordinate where the mouse should click. (x, y)
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to the desired coordinate.
               Default is 0 seconds.
        """
        moveTo(coord[0], coord[1], seconds)
        log_action("moveto", 0)

    @staticmethod
    def center(seconds: int=0) -> None:
        """
        Move the mouse to center. No clicking.
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to the center.
               Default is 0 seconds.
        """
        screen_width, screen_height = size()
        x = round(screen_width / 2)
        y = round(screen_height / 2)
        moveTo(x, y, seconds)
        log_action("moveto-center", 0)

    @staticmethod
    def top(seconds: int=0) -> None:
        """
        Move the mouse to the top. No clicking.
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to the top.
               Default is 0 seconds.
        """
        moveTo(position()[0], 0, seconds)
        log_action("moveto-top", 0)

    @staticmethod
    def bottom(seconds: int=0) -> None:
        """
        Move the mouse to the bottom. No clicking.
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to the bottom.
               Default is 0 seconds.
        """
        screen_width, screen_height = size()
        moveTo(position()[0], screen_height, seconds)
        log_action("moveto-bottom", 0)

    @staticmethod
    def farright(seconds: int=0) -> None:
        """
        Move the mouse to the far right. No clicking.
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to the far right.
               Default is 0 seconds.
        """
        screen_width, screen_height = size()
        moveTo(screen_width, position()[1], seconds)
        log_action("moveto-right", 0)

    @staticmethod
    def farleft(seconds: int=0) -> None:
        """
        Move the mouse to the far left. No clicking.
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to the far left.
               Default is 0 seconds.
        """
        moveTo(0, position()[1], seconds)
        log_action("moveto-left", 0)





