# -*- coding: utf-8 -*-
from pyautogui import position as _position
from app.macro.mouse.click import Click as _Click
from app.macro.mouse.drag import Drag as _Drag
from app.macro.mouse.move import Move as _Move
from app.macro.mouse.moveto import MoveTo as _MoveTo
from app.macro.mouse.scroll import Scroll as _Scroll


def position():
    return (position_x(), position_y())


def position_x():
    return _position()[0]


def position_y():
    return _position()[1]


click = _Click()
drag = _Drag()
move = _Move()
moveto = _MoveTo()
scroll = _Scroll()


