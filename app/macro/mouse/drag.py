# -*- coding: utf-8 -*-
from pyautogui import dragTo

from app.macro.mouse.log import log_action


class Drag:

    @staticmethod
    def coord(coord: tuple, seconds: int=1, button: str='left') -> None:
        """
        Move the mouse to a new coordinate while keeping the mouse button down (drag).
        :param coord: The coordinate where the mouse should go to. (x, y)
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to desired offset.
               Default is 0 seconds.
        :param button: The button that should be hold. Default is 'left'.
        """
        dragTo(coord[0], coord[1], seconds, button=button)
        log_action("dragto", 0)
