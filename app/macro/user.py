# -*- coding: utf-8 -*-
import app
from app.config.ini import Ini, fallback_no_default_folders

import app.config as config
log = config.macro_logger


class User:
    """
    Information about the path of the PHK macro scripts

    Available values:
      * root_folder     = The path where PHK is installed
      * default_folder  = The path to the default script folder
    """
    def __init__(self):
        self.root_folder = app.user.root_folder
        self.default_folder = app.user.default_folder
        ini = Ini()
        self.no_default_folders = ini.read_list_values(
            "no_default_folders",
            fallback=fallback_no_default_folders)

