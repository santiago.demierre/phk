from pydub import AudioSegment
from pydub.playback import play
import os
import time
import app
import app.config as config
log = config.macro_logger


class Play:
    """
    Play different sounds. Useful as a warning when a time consuming operation ends.

    """

    def __init__(self):
        try:
            self._sound_path = os.path.abspath(os.path.join(
                app.system.phk_run_path, "app", "macro", "sound"))
        except IOError:
            self._sound_path = ""
            log.error("Path to sounds could not be found.")

    def ping(self, times: int=1, interval: float=0.5):
        """
        Plays a 'ping' sound.

        :param times: The amount of time the sound will play. Default = 1
        :param interval: The interval in seconds between the sounds. Default = 0.5
        """
        sound = os.path.abspath(os.path.join(self._sound_path, "ping.wav"))
        self._play(sound, times=times, interval=interval)

    def ting(self, times: int=1, interval: float=0.5):
        """
        Plays a 'ting' sound.

        :param times: The amount of time the sound will play. Default = 1
        :param interval: The interval in seconds between the sounds. Default = 0.5
        """
        sound = os.path.abspath(os.path.join(self._sound_path, "ting.wav"))
        self._play(sound, times=times, interval=interval)

    def beep1(self, times: int=1, interval: float=0.5):
        """
        Plays a 'beep' sound.

        :param times: The amount of time the sound will play. Default = 1
        :param interval: The interval in seconds between the sounds. Default = 0.5
        """
        sound = os.path.abspath(os.path.join(self._sound_path, "beep1.wav"))
        self._play(sound, times=times, interval=interval)

    def beep2(self, times: int=1, interval: float=0.5):
        """
        Plays a 'beep' sound.

        :param times: The amount of time the sound will play. Default = 1
        :param interval: The interval in seconds between the sounds. Default = 0.5
        """
        sound = os.path.abspath(os.path.join(self._sound_path, "beep2.wav"))
        self._play(sound, times=times, interval=interval)

    def bell1(self, times: int=1, interval: float=0.5):
        """
        Plays a 'bell' sound.

        :param times: The amount of time the sound will play. Default = 1
        :param interval: The interval in seconds between the sounds. Default = 0.5
        """
        sound = os.path.abspath(os.path.join(self._sound_path, "bell1.wav"))
        self._play(sound, times=times, interval=interval)

    def bell2(self, times: int=1, interval: float=0.5):
        """
        Plays a 'bell' sound.

        :param times: The amount of time the sound will play. Default = 1
        :param interval: The interval in seconds between the sounds. Default = 0.5
        """
        sound = os.path.abspath(os.path.join(self._sound_path, "bell2.wav"))
        self._play(sound, times=times, interval=interval)

    @staticmethod
    def _play(song: str, times: int=1, interval: float=0.5):
        try:
            song = AudioSegment.from_wav(song)
            log.debug(f"Play sound [{times}] times, interval:[{interval}]")
            for i in range(0, times):
                play(song)
                time.sleep(interval)
        except IOError:
            log.error(f"Sound could not be loaded: [{song}]")

