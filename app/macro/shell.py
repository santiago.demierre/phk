# -*- coding: utf-8 -*-
import subprocess
from app.macro.system import System
from app.script.action import run_shell_command, return_shell_command_output

import app.config as config
log = config.macro_logger

"""
Set different commands used on the commandline by Windows and Linux
"""
if System().os_platform() == "windows":
    CMD_WHICH = 'where'
    CMD_CLEAR = 'clear'
    CMD_LESS = 'more'
    CMD_PWD = 'chdir'
    CMD_DATE = 'time'
    CMD_FREE = 'mem'
    CMD_IFCONFIG = 'ipconfig'
    CMD_START = 'start '
else:
    CMD_WHICH = 'which'
    CMD_CLEAR = 'cls'
    CMD_LESS = 'less'
    CMD_PWD = 'pwd'
    CMD_DATE = 'date'
    CMD_FREE = 'free'
    CMD_IFCONFIG = 'ifconfig'
    CMD_START = ''
if System().os_platform() == "mac":
    CMD_START = 'open -a '


class Shell:
    """Shell related functions"""

    def __init__(self) -> None:
        pass

    @staticmethod
    def run_command(cmd: str) -> None:
        """
        Execute a shell command.

        :param cmd: The command to execute.
        """
        log.debug(f"Run shell command: [{cmd}]")
        run_shell_command(cmd)

    @staticmethod
    def return_output(cmd: str) -> str:
        """
        Execute a shell command and return the output.

        :param cmd: The command to execute.
        :return: The output of the command as a string.
        """
        log.debug(f"Return output from: [{cmd}]")
        return return_shell_command_output(cmd)

    @staticmethod
    def executable_found(prog: str) -> bool:
        """
        Checks if an executable program is found.

        :return: True or False
        """
        if System().os_platform() == "mac":
            found = Shell.osx_executable_found(prog)
        else:
            found = subprocess.call([CMD_WHICH, prog],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE) == 0
        if not found:
            log.warning(f"Executable not found: [{prog}]")
        return found

    @staticmethod
    def osx_executable_found(prog: str) -> bool:
        """
        Checks if an executable program is found in OSX.

        :return: True or False
        """
        found = subprocess.call([CMD_WHICH, prog],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE) == 0
        if not found:
            # TODO: find program name in /Applications
            found = True
            log.warning(f"Executable not found: [{prog}]")
        return found


    def start_a_browser(self, url: str, browsers: list=None) -> bool:
        """
            Start or open a webbrowser with an url. This useful when you do not know
            which browser is installed.

            :param url: The url that needs to be opened.
            :param browsers: A list of browsers that can be opened. For each browser in the
                   list, it is first checked if the browser is found.
                   You can change the list and the order according to your preferences.
                   Default = ["firefox", "chromium-browser", "google-chrome", "opera",
                   "microsoft-edge", "iexplore"]
            :return: True if a browser is started, False if no browser can be found.
        """
        if browsers is None:
            browsers = ["firefox", "chromium-browser", "google-chrome", "safari", "opera",
                        "microsoft-edge", "iexplore"]
        found_browser = None
        for browser in browsers:
            if self.executable_found(browser):
                found_browser = browser
                break
        if not found_browser:
            log.error("No browser found.")
            return False
        # Edge has a different way to start from the commandline
        if found_browser == "microsoft-edge":
            cmd = f"{found_browser}:{url}"
        else:
            cmd = f"{CMD_START}{found_browser} {url}"
            log.debug(f"Start browser: [{cmd}]")
        self.run_command(cmd)
        return True




