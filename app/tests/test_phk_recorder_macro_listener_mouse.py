# -*- coding: utf-8 -*-
import pytest
from datetime import datetime, timedelta
from pynput.mouse import Button
from app.phk.recorder import MacroListener

stop_keys = "<ctrl><alt>-"


def test__time_diff_in_seconds() -> None:
    ml = MacroListener(stop_keys)
    ml.active_listener = True
    ml.old_position_time = datetime.now() - timedelta(seconds=3)
    assert ml._time_diff_in_seconds() == 3
    ml.old_position_time = datetime.now() - timedelta(seconds=1)
    assert ml._time_diff_in_seconds() == 1
    ml.old_position_time = datetime.now() - timedelta(seconds=0.2)
    assert ml._time_diff_in_seconds() == 0


def test__is_mouse_hovering() -> None:
    ml = MacroListener(stop_keys)
    ml.active_listener = True
    ml.old_position_time = datetime.now() - timedelta(seconds=3)
    assert ml._is_mouse_hovering(min_secs=5) is False
    ml.old_position_time = datetime.now() - timedelta(seconds=5)
    assert ml._is_mouse_hovering(min_secs=5) is True
    ml.old_position_time = datetime.now() - timedelta(seconds=8)
    assert ml._is_mouse_hovering(min_secs=5) is True


def test__on_move_no_hover_old_position_changes() -> None:
    ml = MacroListener(stop_keys)
    ml.active_listener = True
    ml.old_position = (0, 0)
    ml.old_position_time = datetime.now() - timedelta(seconds=2)
    ml._on_move(100, 100)
    assert ml.old_position == (100, 100)
    assert ml.commands == []


def test__on_move_hover_old_position_stays_the_same() -> None:
    ml = MacroListener(stop_keys)
    ml.active_listener = True
    ml.old_position = (0, 0)
    ml.old_position_time = datetime.now() - timedelta(seconds=8)
    ml._on_move(100, 100)
    assert ml.old_position == (0, 0)
    assert ml.commands == ['m.mouse.moveto.coord((100, 100), seconds=1)']


def test__on_click() -> None:
    ml = MacroListener(stop_keys)
    ml.active_listener = True
    ml.old_position = (0, 0)
    ml._on_click(x=100, y=100, button=Button.left, pressed=True)
    assert ml.old_position == (100, 100)
    assert ml.commands == ['m.mouse.click.left((100, 100), clicks=1, wait=0.5)']
