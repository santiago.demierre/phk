# -*- coding: utf-8 -*-
import pprint
import pytest
from pynput.keyboard import KeyCode, Key
from app.phk.keyboard_listener import KeyboardListener

pp = pprint.PrettyPrinter(indent=4)


@pytest.mark.skip(
    reason="When testing is done automatically,"
    "this will cause a problem in Linux"
    "because the display manager will run"
    "out of connections."
)
def test_start_stop():
    kl = KeyboardListener()
    kl.start()
    assert kl.active_listener is True
    kl.stop()
    assert kl.active_listener is False


def test_activate_deactivate() -> None:
    kl = KeyboardListener()
    kl.activate()
    assert kl.active_listener is True
    kl.deactivate()
    assert kl.active_listener is False


def test_current_active_key() -> None:
    kl = KeyboardListener()
    # add one key
    kl._add_current_active_key(Key.shift)
    assert kl.current_active_keys == {Key.shift}
    # and another
    kl._add_current_active_key(Key.alt)
    result = kl.current_active_keys
    expected = {Key.shift, Key.alt}
    assert all([a == b for a, b in zip(result, expected)])
    # and now a normal character
    normal_char = KeyCode.from_char("a")
    kl._add_current_active_key(normal_char)
    result = kl.current_active_keys
    expected = {Key.shift, Key.alt, normal_char}
    assert all([a == b for a, b in zip(result, expected)])
    # remove the last one
    kl._remove_current_active_key(normal_char)
    result = kl.current_active_keys
    expected = {Key.shift, Key.alt}
    assert all([a == b for a, b in zip(result, expected)])
    # remove them all
    kl._remove_all_current_active_keys()
    assert kl.current_active_keys == set()


def test__get_dtm_key():
    kl = KeyboardListener()
    assert kl.dtm_key_code is None
    kl._get_dtm_key()
    assert kl.dtm_key_code is not None
