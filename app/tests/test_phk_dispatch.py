# -*- coding: utf-8 -*-
import app

from app.phk.dispatch import Dispatch


def test_actions():
    dp = Dispatch()
    assert dp.actions((app.action.run, "<cmd>", "ls", "Run ls")) is True
    assert dp.actions(("asdf", "<cmd>", "ls", "Run ls")) is False
    assert dp.actions((app.action.run, "<cmd>")) is False
