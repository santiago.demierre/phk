# -*- coding: utf-8 -*-
from app.phk.key_convert import KeyConvert
from pynput.keyboard import Key, KeyCode

kv = KeyConvert()

def test__is_key_representation() -> None:
    assert kv._is_key_representation("ENTER") is True
    assert kv._is_key_representation("enter") is False
    assert kv._is_key_representation("TAB") is True
    assert kv._is_key_representation("SPACE") is True
    assert kv._is_key_representation("QQQQ") is False
    assert kv._is_key_representation("1235") is False

def test__clean_key_representation() -> None:
    assert kv._clean_key_representation("<enter>") == "ENTER"
    assert kv._clean_key_representation("enter>") == "ENTER"
    assert kv._clean_key_representation("<enter") == "ENTER"
    assert kv._clean_key_representation("enter") == "ENTER"
    assert kv._clean_key_representation("< enter >") == "ENTER"
    assert kv._clean_key_representation("< ENTER >") == "ENTER"
    assert kv._clean_key_representation("<qqqqq>") == "QQQQQ"

def test_get_key_code() -> None:
    assert kv.get_key_code("<space>") is Key.space
    assert kv.get_key_code("<enter>") is Key.enter
    assert kv.get_key_code("< enter >") is Key.enter
    assert kv.get_key_code("enter") is Key.enter
    assert kv.get_key_code("ENTER") is Key.enter
    assert kv.get_key_code("[ENTER]") is False
    assert kv.get_key_code("a") is False
    assert kv.get_key_code("2") is False
    assert kv.get_key_code("@") is False

def test_get_key_representation() -> None:
    assert kv.get_key_representation(Key.space) == "<space>"
    assert kv.get_key_representation(Key.tab) == "<tab>"
    assert kv.get_key_representation(Key.f5) == "<f5>"
    assert kv.get_key_representation(Key.up) == "<up>"
    assert kv.get_key_representation("a") == "a"
    assert kv.get_key_representation("A") == "A"

def test_get_hotkey_keys() -> None:
    expected = [Key.ctrl, Key.alt, KeyCode.from_char('3')]
    result = kv.get_hotkey_keys("<ctrl><alt>3")
    assert result == expected
    #assert all([a == b for a, b in zip(result, expected)])


def test_is_special_key() -> None:
    assert kv.is_special_key("a") is False
    assert kv.is_special_key("<tab>") is True

def test_is_single_character() -> None:
    assert kv.is_single_character("a") is True
    assert kv.is_single_character("4") is True
    assert kv.is_single_character("&") is True
    assert kv.is_single_character("<tab>") is False

def test_lowercase_char() -> None:
    assert kv.lowercase_char("A") == KeyCode.from_char("a")
    assert kv.lowercase_char("a") == KeyCode.from_char("a")
    assert kv.lowercase_char("2") == KeyCode.from_char("2")
    assert kv.lowercase_char("$") == KeyCode.from_char("$")
    assert kv.lowercase_char(Key.enter) == Key.enter
    assert kv.lowercase_char(Key.space) == Key.space

def test_count_characters() -> None:
    assert kv.count_characters("a") == 1
    assert kv.count_characters("ab") == 2
    assert kv.count_characters("<ctrl>a") == 1
    assert kv.count_characters("<ctrl><shift>a") == 1
