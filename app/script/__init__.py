# -*- coding: utf-8 -*-
import os
import app

Run = app.action.run
RunPython = app.action.python
Type = app.action.type
Expand = app.action.expand
ReturnOutput = app.action.return_output


def this_folder(file):
    return os.path.dirname(os.path.realpath(file))


def this_file(file):
    return os.path.realpath(file)
