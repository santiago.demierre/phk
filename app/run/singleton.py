# -*- coding: utf-8 -*-
"""
This is a copy of tendo/singleton
https://github.com/pycontribs/tendo/blob/master/tendo/singleton.py

Instead of just using it I have adjusted it to:
- use my own logger
- change the error messages
- print the PID of the running process to provide the info to kill the running process.
"""
import app.config as config
import os
import sys
import tempfile
if sys.platform != "win32":
    import fcntl

log = config.default_logger


class SingleInstanceException(BaseException):
    pass


class SingleInstance(object):

    def __init__(self, flavor_id="", lockfile=""):
        self._set_lockfile(lockfile, flavor_id)
        self._get_current_pid()
        self._get_running_pid()
        self.initialized = False
        if sys.platform == 'win32':
            try:
                # file already exists, we try to remove (in case previous
                # execution was interrupted)
                if os.path.exists(self.lockfile):
                    os.unlink(self.lockfile)
                self.fd = os.open(
                    self.lockfile, os.O_CREAT | os.O_EXCL | os.O_RDWR)
                self._write_pid_to_lockfile()
            except OSError:
                type, e, tb = sys.exc_info()
                if e.errno == 13:
                    self._raise_error_and_pid()
                    raise SingleInstanceException()
                print(e.errno)
                raise
        else:  # non Windows
            self.fp = open(self.lockfile, 'w')
            self.fp.flush()
            self._write_pid_to_lockfile()
            try:
                fcntl.lockf(self.fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
            except (IOError, BlockingIOError):
                self._raise_error_and_pid()
                raise SingleInstanceException()
        self.initialized = True

    def __del__(self):
        if not self.initialized:
            return
        try:
            if sys.platform == 'win32':
                if hasattr(self, 'fd'):
                    os.close(self.fd)
                    os.unlink(self.lockfile)
            else:
                fcntl.lockf(self.fp, fcntl.LOCK_UN)
                # os.close(self.fp)
                if os.path.isfile(self.lockfile):
                    os.unlink(self.lockfile)
        except Exception as e:
            if log:
                log.warning(e)
            else:
                print("PHK unloggable error: %s" % e)
            sys.exit(-1)

    def _write_pid_to_lockfile(self):
        with open(self.lockfile, "w") as file:
            if self.running_pid:
                file.write(self.running_pid)
            else:
                file.write(self.current_pid)

    def _get_current_pid(self):
        self.current_pid = str(os.getpid())
        log.debug("PHK PID = {}".format(self.current_pid))

    def _get_running_pid(self):
        self.running_pid = None
        if os.path.exists(self.lockfile):
            with open(self.lockfile, "r") as file:
                self.running_pid = file.read()

    def _raise_error_and_pid(self):
        self._get_running_pid()
        log.error(f"PHK is already running with PID:{self.running_pid}")

    def _set_lockfile(self, lockfile, flavor_id):
        if lockfile:
            self.lockfile = lockfile
        else:
            basename = os.path.splitext(os.path.abspath(sys.argv[0]))[0].replace(
                "/", "-").replace(":", "").replace("\\", "-") + '-%s' % flavor_id + '.lock'
            self.lockfile = os.path.normpath(
                tempfile.gettempdir() + '/' + basename)
        log.debug("PHK lockfile: " + self.lockfile)
