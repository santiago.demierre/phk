# -*- coding: utf-8 -*-
import app
from app.run.args import get_args
from app.run.singleton import SingleInstance, SingleInstanceException
import app.config as config

log = config.default_logger

args = get_args()


def run_once(name):
    try:
        run = SingleInstance(flavor_id=name)
        from phk import Phk

        phk = Phk()
        phk.set_userscripts_folder(args.user_scripts_folder)
        log.debug(f"OS:{app.system.os_name,}, Python:{app.system.python_version}")
        phk.start()
        del run
    except SingleInstanceException:
        if args.reload:
            # TODO: Make it possible to reload PHK
            pass
        else:
            pass
        pass
