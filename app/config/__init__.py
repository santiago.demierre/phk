# -*- coding: utf-8 -*-
import logging
from app.config.log import Log
from app.config.log import default_logger_name
from app.config.log import macro_logger_name
from app.config.log import macro_recorder_name

default_log = Log(default_logger_name)
default_logger = default_log.get_logger()

macro_log = Log(macro_logger_name)
macro_log.set_file_format("[%(asctime)-13s %(levelname)-8s] %(message)s")
macro_log.set_stream_format("%(message)s")
macro_log.set_stream_level(logging.CRITICAL)
macro_log.set_file_level(logging.DEBUG)
macro_logger = macro_log.get_logger()


macro_recorder = Log(macro_recorder_name)
macro_recorder.set_file_format("%(message)s")
macro_recorder.set_stream_format("%(message)s")
macro_recorder.set_stream_level(logging.CRITICAL)
macro_recorder.set_file_level(logging.DEBUG)
macro_recorder = macro_recorder.get_logger()
