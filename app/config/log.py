# -*- coding: utf-8 -*-
import os
import logging
from logging.handlers import RotatingFileHandler
import app

default_logger_name = "phk"
macro_logger_name = "macro"
macro_recorder_name = "recorded"
"""
Set logging configuration
"""


class Log:
    def __init__(self, name: str = default_logger_name):
        self.name = name
        self.set_stream_format("%(message)s")
        self.set_stream_level(logging.ERROR)
        self.set_file_format(
            "[%(asctime)s %(levelname)-6s %(lineno)s:%(filename)s.%(funcName)-20s] %(message)s "
        )
        self.set_file_level(logging.DEBUG)
        self.set_file_name(name)

    def set_stream_level(self, level: int):
        self.stream_level = level

    def set_stream_format(self, fmt: str):
        self.stream_format = fmt

    def set_file_level(self, level: int):
        self.file_level = level

    def set_file_format(self, fmt: str):
        self.file_format = fmt

    def set_file_name(self, name: str):
        self.filename = f"{name}.log"

    def get_logger(self) -> logging.getLogger:
        # Re-use loggers when possible otherwise multiple handlers will be active
        if self.name not in logging.root.manager.loggerDict:
            self.mylogger = logging.getLogger(self.name)
            # Default always to DEBUG
            self.mylogger.setLevel(logging.DEBUG)
            if self.name == default_logger_name:
                self.set_phk_streamhandler()
                self.set_phk_filehandler()
            if self.name == macro_logger_name:
                self.set_macro_streamhandler()
                self.set_macro_filehandler()
            if self.name == macro_recorder_name:
                self.set_macro_recorder_filehandler()
        else:
            self.mylogger = logging.getLogger(self.name)
        return self.mylogger

    def set_phk_streamhandler(self) -> None:
        stream = logging.StreamHandler()
        stream.setLevel(self.stream_level)
        formatter = logging.Formatter(self.stream_format)
        stream.setFormatter(formatter)
        self.mylogger.addHandler(stream)

    def set_phk_filehandler(self) -> None:
        file = RotatingFileHandler(
            filename=os.path.join(app.system.phk_run_path, self.filename),
            mode="a",
            maxBytes=5 * 1024 * 1024,
            backupCount=2,
            encoding=None,
            delay=0,
        )
        file.setLevel(self.file_level)
        formatter = logging.Formatter(self.file_format, "%Y-%m-%d %H:%M:%S")
        file.setFormatter(formatter)
        self.mylogger.addHandler(file)

    def set_macro_streamhandler(self) -> None:
        stream = logging.StreamHandler()
        stream.setLevel(self.stream_level)
        formatter = logging.Formatter(self.stream_format)
        stream.setFormatter(formatter)
        self.mylogger.addHandler(stream)

    def set_macro_filehandler(self) -> None:
        """This logfile is overwritten everytime a macro runs"""
        filename = os.path.join(
            app.system.phk_run_path, app.user.root_folder, self.filename
        )
        file = logging.FileHandler(filename, mode="w")
        file.setLevel(self.file_level)
        formatter = logging.Formatter(self.file_format)
        file.setFormatter(formatter)
        self.mylogger.addHandler(file)

    def set_macro_recorder_filehandler(self) -> None:
        """This file is overwritten everytime the macro recorder runs"""
        filename = os.path.join(
            app.system.phk_run_path,
            app.user.root_folder,
            f"{macro_recorder_name}.py",
        )
        file = logging.FileHandler(filename, mode="w")
        file.setLevel(self.file_level)
        formatter = logging.Formatter(self.file_format)
        file.setFormatter(formatter)
        self.mylogger.addHandler(file)
