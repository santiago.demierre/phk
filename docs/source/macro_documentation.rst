:doc:`[Back to Index] <index>`

PHK macros
==========
A PHK macro is just a regular Python file that is triggered by a hotkey and makes use of the PHK macro library.

Example macro:
--------------
An example macro could look like this:

.. code-block:: python3
   :linenos:

    import app.macro as m
    from app.script import RunPython, this_file

    THIS_FILE = this_file(__file__)
    hotkeys = []

    hotkeys.append([RunPython, "<ctrl><alt>3", THIS_FILE, "Example"])

    def main():
        m.wait.seconds(2)
        m.keys.press("<f11>")
        # Pause the macro until the ENTER key is pressed
        m.wait.for_key_press("<enter>")
        m.mouse.moveto.center()
        m.mouse.move.down(20)
        m.mouse.click.left(clicks=3, interval=0.5)
        m.mouse.move.relative((100, -100), seconds=2)
        x = m.mouse.position_x
        y = m.mouse.position_y
        m.log.info(f"Mouse position: x={x} y={y}")
        text = "line 1<enter>line2<tab>more line 2<enter>line3<enter>"
        m.clipboard.copy(text)
        m.log.info(f"Content of the clipboard:{m.clipboard.get()}")
        m.keys.type(text)
        m.keys.press("<esc>")
        m.log.debug("Clicked the ESC key")

    if __name__ == "__main__":
        main()

Explanation:
  * Line 1: The macro library is imported as **m**
  * Line 2: **RunPython** is imported, this is used on line 7 as the action for the hotkey
  * Line 4: We want to run the current Python file as a macro. On line 7 this is used as the path of the file to execute.
  * Line 7: Here we define the hotkey. This could also be defined in another file, but this is much more convenient.
  * Line 10-26: This is the actual macro. See the documentation below for each macro command.
  * Line 28-29: Since we want to execute this file we need to have the "__main__" block with the function to execute

Look in the `scripts/example <https://gitlab.com/ronspe/phk/-/blob/master/scripts/example>`_ folder for more examples of macros.

Macro logfile:
--------------
| When a macro is executed, there is also a log created. This is useful for debugging.
| On line 20, 23 and 26 we write to the log file but most macro functions will write debugging info to the logfile.
| The logfile is located in the `scripts` folder with the name `macro.log`.
| The logfile is overwritten everytime a macro is executed.

The logfile looks something like this:

.. code-block:: text

    [2020-03-03 14:18:53,822 DEBUG   ] Press and release: [<tab>]
    [2020-03-03 14:18:54,324 DEBUG   ] Wait for 0.5 seconds
    [2020-03-03 14:18:54,325 DEBUG   ] Run shell command: [copyq menu]
    [2020-03-03 14:18:54,582 DEBUG   ] Wait for 0.25 seconds
    [2020-03-03 14:18:55,618 DEBUG   ] Press and release: [<tab>]
    [2020-03-03 14:18:56,121 DEBUG   ] Wait for 0.5 seconds
    [2020-03-03 14:18:56,121 DEBUG   ] Run shell command: [vim]
    [2020-03-03 14:18:56,379 DEBUG   ] Wait for 0.25 seconds
    [2020-03-03 14:18:57,445 DEBUG   ] Press and release: [<esc>]
    [2020-03-03 14:18:57,446 DEBUG   ] Typing string: [gg]



Macro recorder
--------------
| In the `scripts/default` folder is a script named: `macro_recorder.py`.
| This script will start a macro recorder that will record your keyboard and mouse activity.

Once you stop the recorder it will write the recorded macro to: `recorded.py` also in the `default` folder.

| The macro recorder will use the file `_recorded.template` as a template to write the macro to `recorded.py`.
| You can change the `_recorded.template` file if you like.

This template uses only two variables:
  * **$play_key** : this holds the hotkey to execute the macro (default = <ctrl><alt>0")
  * **$script** : this holds the recorded macro

Keys to record, stop recording and playing the macro:
  * start_keys = "<ctrl><alt>="
  * stop_keys = "<ctrl><alt>-"
  * play_keys = "<ctrl><alt>0"

| These hotkeys are defined in the `macro_recorder.py` file and can be changed to your liking.
| When the macro recorder starts you will see a pop-up window that shows the key combination that you have configured to stop the recording of the macro.
| When the macro recorder stops you will see a pop-up window that shows location of the recorded macro file.

Once you have recorded a macro and you would like to save it for future use:
  #. Rename the file `recorded.py` to something else, otherwise it will be overwritten once the macro recorder will be started again.
  #. Change the hotkey, otherwise a future recorded macro will use the same hotkey ("<ctrl><alt>0")
  #. Change, add and remove parts of your script.


| **Tip1**: When recording a macro; don't type too fast.
| If you accedently hit two keys at almost the same time it could be recorded as a "press" event and not as a "type" event.
| This is usually not a big problem, but the script will need some cleaning up afterwards.
| This is the difference:

.. code-block:: python3

        # This is better:
        m.keys.type("abcd")

        # This is not readable:
        m.keys.press("a")
        m.keys.press("b")
        m.keys.press("c")
        m.keys.press("d")


| **Tip2**: Add some wait time to your scripts.
| When pressed keys are played back it is usually too fast for an application to react properly and you will not get the desired results.
| PHK has already build-in a short delay when playing back a pressed key which will solve most of these problems.
| With web applications you might want to add some extra wait time at some points in your script.

| **Tip3**: Change the location of the popups when the recording starts and stops
| In the file `scripts/config.ini` you can change the following settings:
| - The position on the screen for the popup windows.
| - The time-out in seconds for the popup that shows when the macro recorder starts.

Documentation of macro commands
-------------------------------

.. toctree::
    :maxdepth: 2
    :glob:

    macro/*

GUI functions in macros
-----------------------
| There are no GUI functions in the macro library anymore.
| It is much better to use the `pySimpleGUI <https://pysimplegui.readthedocs.io/en/latest/>`_ library for any GUI interactions.
| Since this is already installed and available it is easy to implement in your macro scripts:

.. code-block:: python3

    import PySimpleGUI as sg
    sg.Popup('Hello From PySimpleGUI!', 'This is the shortest GUI program ever!')

In the file `scripts/examples/wait_for_me.py <https://gitlab.com/ronspe/phk/-/blob/master/scripts/example/wait_for_me.py>`_ you can see how this can be used.

:doc:`[Back to Index] <index>`
