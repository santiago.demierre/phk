.. PythonHotKey documentation master file, created by
   sphinx-quickstart on Sat Feb 29 12:03:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PythonHotKey's (PHK) documentation!
===================================
Macro commands:
===============

.. autoclass:: app.macro.clipboard.Clipboard
   :members:
.. autoclass:: app.macro.input.Input
   :members:

.. toctree::
    :maxdepth: 2
    :caption: Contents


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
